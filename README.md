# LibreHealth's Data Scraper

## About

This Scraper is made with Python's most popular Open Source framework **[Scrapy](https://scrapy.org/)** and **[Selenium](https://www.selenium.dev/selenium/docs/api/py/)**.

- **Scrapy** - An Open Source framework for extracting the data from websites 
- **Selenium** - The selenium package is used to automate web browser interaction from Python.
With Selenium webdriver this scraper can also scrap dynamic websites.

This scraper scraps chargemaster of US Hospitals and also process them uniformly to have proper columns.

Available Data of States
- Alaska
- Indiana
- New York
- Medicare Data Available for all US States

### Code Organisation

- scrap_cdm - This is scrapy project folder , it contains a spider directory which contains scrapy spider to crawl data from web
- Data - This Directory will be generated when user scraps any data using scrapy crawl commands. It will contain scraped data.
- CDM - This will also be generated when user will process data using "python run_processes.py" command

## Getting Started

#### Installing Chromedriver [(Link)](https://chromedriver.storage.googleapis.com/index.html?path=83.0.4103.39/)

##### macOS
```shell
brew install brew-cask
brew cask install chromedriver
```
##### Linux

```shell
apt-get update
apt-get install -y libxss1 libappindicator1 libindicator7
apt-get install unzip
wget -N https://chromedriver.storage.googleapis.com/83.0.4103.39/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
chmod +x chromedriver
mv -f chromedriver /usr/local/share/chromedriver
ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
ln -s /usr/local/share/chromedriver /usr/bin/chromedriver
```
##### Windows

1. Download directly from **[(Here)](https://chromedriver.storage.googleapis.com/index.html?path=83.0.4103.39/)**
2. Place "chromedriver.exe" path in System's Environment variable "PATH"

#### Installing ChromeBrowser [(Link)](https://www.google.com/intl/en_in/chrome/)

##### macOS

```shell
brew cask install google-chrome
```    

##### Linux
    
```shell
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt install -y ./google-chrome-stable_current_amd64.deb
mv /usr/bin/google-chrome-stable /usr/bin/google-chrome
```    

##### Windows

Download directly from **[(Here)](https://www.google.com/intl/en_in/chrome/)**

##### NOTE:- Make sure chromedriver & Google Chrome version is same

#### Installing MDBTOOLS [Optional, if you want to process CDM of Indiana, then install it]

##### macOS

```shell
brew install mdbtools
```    

##### Linux
    
```shell
apt-get install -y python-dev 
apt-get install -y unixodbc-dev
apt-get install -y mdbtools odbc-mdbtools
```    
##### Windows

1. Open "Process CDM/Indiana/process.py" File
2. Replace usage of "mdb_to_pandas" function to "mdb_to_pandas_windows"
3. Make sure Microsoft Access Drivers are installed

### Remove the following Code from scrapCDM/middlewares.py file

```python
display = Display(visible=0, size=(800, 800))
display.start()
```
This is used in CI Pipeline, because there is no display output for the browser to launch in. 

**Important:- You need to remove these two lines**

#### Installing requirements.txt

```shell
pip install -r requirements.txt
```

#### Scrapping CDM & Processing

Run these commands after opening project directory in terminal

|           **Command**             | **Link**          | **State Name**       |  **Description** | 
|-----------------------------------|-------------------|----------------------|------------------|
|  scrapy crawl example | [**(Link)**](scrap_cdm/urls.py) | Alaska | This scraps Alaska Hospitals CDM whose urls are saved in urls.py file| 
|  scrapy crawl indiana | [**(Link)**](https://www.in.gov/isdh/28202.htm) | Indiana | Scraps Indiana Hospitals CDM.<br> This Data is provided by Indiana State Health Department|
|  scrapy crawl new_york | [**(Link)**](https://health.data.ny.gov/Health/Hospital-Inpatient-Cost-Transparency-Beginning-200/7dtz-qxmr) | New York |Scraps New York Hospitals CDM.<br> This Data is provided by New York State Health Department|
|  scrapy crawl california | [**(Link)**](https://data.chhs.ca.gov/dataset/chargemasters) | California |Scraps California hospitals CDM. <br> This Data is provided by OSHPD California State Health Department|
|  scrapy crawl medicare | [**Inpatient Data of 3000 Hospitals**](https://data.cms.gov/Medicare-Inpatient/Inpatient-Prospective-Payment-System-IPPS-Provider/tcsp-6e99)<br>[**Outpatient Data of 3000 Hospitals**](https://data.cms.gov/Medicare-Outpatient/Provider-Outpatient-Hospital-Charge-Data-by-APC-CY/fmbt-qrrw)| Scraps Data of all remaining states of US  | This Data is provided by medicare, it contains data of all states|

### Processing Scraped Data to proper columns

```shell
python run_processes.py
```

This command will run "process.py" script which is different for each hospital & is present in each hospital's directory already.

After this command is executed all parsed data will be saved in "CDM" directory statewise & with proper column names.


#### Naming Convention used

Each process.py script will create a .csv file which has three columns 

- Charge - price of procedure
- Description - description of procedure
- Category - It can be Standard, DRG, Pharmacy


## Branch Policy

We have the following branches
* **develop** All development goes on in this branch. If you're making a contribution, you are supposed to make a pull request to _development_. PRs to development branch must pass a build check on gitlab CI Pipeline.
* **master** This contains shipped code. After significant features/bugfixes are accumulated on development, we make a version update and make a release.


## Communication

The Cost Of Care chat channel is on [LibreHealth Forums.](https://forums.librehealth.io/t/project-develop-an-android-mobile-application-to-show-patient-friendly-costs-of-care/3685/103)

## Contributions Best Practices

Please help us follow the best practice to make it easy for the reviewer as well as the contributor. We want to focus on the code quality more than on managing pull request ethics.

* Single commit per pull request
* Reference the issue numbers in the commit message. Follow the pattern ``` Fixes #<issue number> <commit message>```
* Follow uniform design practices. The design language must be consistent throughout the app.
* The pull request will not get merged until and unless the commits are squashed. In case there are multiple commits on the PR, the commit author needs to squash them and not the maintainers cherrypicking and merging squashes.
* If the PR is related to any front end change, please attach relevant screenshots in the pull request description.
* Before you join development, please set up the project on your local machine, run it and go through the application completely. Press on any button you can find and see where it leads to. Explore.
* If you would like to work on an issue, drop in a comment at the issue. If it is already assigned to someone, but there is no sign of any work being done, please free to start working on it.

## Maintainers and Developers

* [**Mua N. Laurent**](https://gitlab.com/muarachmann) 

* [**Darshpreet Singh**](https://gitlab.com/Darshpreet2000)

* [**Judy Gichoya**](https://gitlab.com/judywawira)

* [**Saptarshi Purkayastha**](https://gitlab.com/sunbiz)

* [**Robby O Connor**](https://gitlab.com/robbyoconnor)

## License

This project is licensed under the GNU General Public License v3.0. A copy of [LICENSE](LICENSE) is to be present along with the source code. To obtain the software under a different license, please contact LibreHealth.
